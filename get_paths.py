import contextlib
import os
import subprocess
import sqlite3
import winreg


def get_desktop_path():
    # get Desktop path
    with winreg.OpenKey(
        winreg.HKEY_CURRENT_USER,
        (
            r"Software\Microsoft\Windows\CurrentVersion"
            r"\Explorer\User Shell Folders"
        ),
    ) as key:
        value = winreg.QueryValueEx(key, "Desktop")
        # returns: ('C:\\Users\\<USERNAME>\\OneDrive\\Desktop', 2)
        #           or  ('%USERPROFILE%\\Desktop', 2)
    resolved_win_vars = subprocess.run(
        "cmd.exe /c echo {}".format(value[0]),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,
    )
    desktop_path = resolved_win_vars.stdout.decode("utf-8").strip()

    desktop_folder = os.path.join(desktop_path, "FFXI")

    # ensure desktop folder exists
    if not os.path.exists(desktop_folder):
        os.mkdir(desktop_folder)
    print("Desktop destination folder: {}".format(desktop_folder))  # DEBUG
    return desktop_folder


def get_ffxi_instalation_path():
    # get FFXI instalation path
    with winreg.OpenKey(
        winreg.HKEY_LOCAL_MACHINE,
        r"SOFTWARE\WOW6432Node\PlayOnlineUS\InstallFolder"
    ) as key:
        value = winreg.QueryValueEx(key, "0001")
        # returns: ('C:\\Program Files (x86)\\PlayOnline\\'
        #           'SquareEnix\\FINAL FANTASY XI\\', 1)
    instalation_path = value[0]

    print("FFXI Instalation folder: {}".format(instalation_path))  # DEBUG
    return instalation_path


def get_google_drive_path():
    # get Google Drive config path
    resolved_appdata_local = subprocess.run(
        "cmd.exe /c echo {}".format(r"%LocalAppData%"),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,
    )
    google_drive_config_path = os.path.join(
        resolved_appdata_local.stdout.decode("utf-8").strip(),
        "Google",
        "Drive",
        "user_default",
    )

    # ensure Google Drive is installed
    if not os.path.exists(google_drive_config_path):
        print(
            "[ERROR] Google Drive is not installed."
            " {} does not exist.".format(google_drive_config_path)
        )
        quit()
    # get Google Drive sync path
    with contextlib.closing(
        sqlite3.connect(os.path.join(google_drive_config_path,
                                     "sync_config.db"
                                     )
                        )
    ) as conn:
        c = conn.cursor()
        c.execute("SELECT data_value "
                  "FROM data "
                  "WHERE entry_key='local_sync_root_path';")
        sync_path = c.fetchall()[0][0]
        # returns '\\\\?\\C:\\Users\\<USERNAME>\\Google Drive'
        google_drive_sync_path = sync_path.strip("\\").strip("?").strip("\\")
    # C:\Users\<USERNAME>\AppData\Local\Google\Drive\user_default\sync_config.db
    #   table: data
    #     entry_key: local_sync_root_path
    #     data_key: value
    #     data_value: \\?\C:\Users\<USERNAME>\Google Drive
    # # #
    #     entry_key: root_config__0
    #     data_key: rowkey
    #     data_value: \\?\C:\Users\<USERNAME>\Google Drive
    # Z:\>echo %LocalAppData%
    # C:\Users\<USERNAME>\AppData\Local

    print("Google Drive folder: {}".format(google_drive_sync_path))  # DEBUG
    return os.path.join(google_drive_sync_path, "FFXI")
    # os.path.join(str(Path.home()), "Google Drive", "FFXI")


def paths():
    paths_dict = dict()
    paths_dict["desktop_folder"] = get_desktop_path()
    paths_dict["ffxi_instalation_folder"] = get_ffxi_instalation_path()
    paths_dict["google_drive_folder"] = get_google_drive_path()
    print("=" * 80)  # DEBUG

    return paths_dict
