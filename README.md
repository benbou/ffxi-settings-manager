


# FFXI Settings Manager

FFXI Settings Manager allows the user to backup, restore and keep *FFXI* files in sync (with *Google Drive*).

<sup>It also works with any 3rd party tool placed in the Desktop FFXI folder.</sup>

## Getting Started

This project come with two executable.

* One to backup the new files (use it before playing on another computer; or after every play session, if you prefer).

* Another one that copies the new files to the Desktop FFXI folder **and** to the FFXI installation folder (use it before starting the game).

### Prerequisites

* To use this software you will need the "*Backup and Sync from Google*" (the *Google Drive* app for *Windows*) installed.
[Direct download page](https://www.google.com/drive/download/thankyou/)
<sup>The Google Drive sync folder **must** be located on the same drive than your Desktop (it's needed to avoid duplication of data - it uses hard links).</sup>

* *FFXI* also need to be installed (obviously).

### Installing

The two executable can be double clinked from anywhere, no installation needed.

> However, it's a good idea to move them into the Desktop FFXI folder, so they are backed up and synced between your devices.

It's also possible to run the Python script source code directly. To do so, the user **must** run the script from an admin command prompt (needed to write in the FFXI installation folder).

Example:
```
python _get_new_files_to_desktop.py
python C:\<full path to the file>\_get_new_files_to_desktop.py
```

## How it work

#### FFXI_backup_to_GoogleDrive

* If the Desktop FFXI folder does not exist, it will be created.

* The program will scan two specific sub folder in the FFXI installation folder ("*USER*" and "*ffxiuser*"), if they exist.

* It will then copy every files and folders that are not yet present in the Desktop FFXI folder; the other files will be compared with their last modification time (older files will be replaced in the Desktop FFXI folder, the rest will be ignored).

* It will finally scan the Desktop FFXI folder and will create a hard link for every files that are not already in the Google drive folder.

* At this point, "*Backup and Sync from Google*" (the *Google Drive* app for *Windows*) should do it's thigh and backup everything new to your *Google Drive* cloud.

#### FFXI_get_new_files_to_desktop

* Since this executable need to write in the FFXI installation folder, it will trigger a UAC prompt to use the required administrative privileges.

* If the Desktop FFXI folder does not exist, it will be created.

* The program will scan the Google drive folder and will create a hard link for every file that are not already in the Desktop FFXI folder.

* It will then scan two specific sub folder in the *Google Drive* folder ("*USER*" and "*ffxiuser*"), if they exist.

* It will finally copy every files and folders that are not yet present in the FFXI installation folder; the other files will be compared with their last modification time (older files will be replaced in the FFXI installation folder, the rest will be ignored).

### What is a hard link

A *hard link* is the file system representation of a file by which more than one path references a single file in the same volume.

Any changes to that file are instantly visible to applications that access it through the hard links that reference it.

https://docs.microsoft.com/en-us/windows/win32/fileio/hard-links-and-junctions

### How I use it

* I mainly play on two computers, my home PC and my workstation at my job.

* The first time I used my program:
  * I used "*FFXI_backup_to_GoogleDrive*" to create my Desktop FFXI folder and populate it with files from the game.
  * Then, I moved my whole *Windower* folder into the FFXI folder on my Desktop (that way all my addons and my settings are backed up and synced between my computers).
  * I also keep *[Altana View](https://github.com/mynameisgonz/AltanaView)*, *[Mappy](https://github.com/KenshiDRK/mappy--Kenshi-Version)*, *[POLUtils](https://github.com/Windower/POLUtils/releases)*, a portable *[Hex Editor](https://mh-nexus.de/en/downloads.php?product=HxD20)* and my [software](https://gitlab.com/benbou/ffxi-settings-manager) (and the source code) in there; so those tool (and their settings) are always with me.

* Before every play session I run "*FFXI_get_new_files_to_desktop*" and every time I quit the game I run "*FFXI_backup_to_GoogleDrive*".

* I also run "*FFXI_backup_to_GoogleDrive*" after every modification to my *GearSwap* files... It's only needed when new files are created, but it's easier for me to just don't think about it and run the program. ;)

* Doing it that way allows me to always have the latest files available on every machine that I use.
And if I ever need to use another PC; all the files I need are right there, in my *Google Drive* cloud.

## Authors

* **Benoit Boulanger** - *Initial work* - [Benbou](https://gitlab.com/benbou)

See also the list of [contributors](https://gitlab.com/benbou/ffxi-settings-manager/-/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Thanks to Preston Landers ([admin.py](admin.py))
