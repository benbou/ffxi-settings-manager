cd /D "%~dp0"

pyinstaller -F --icon="%~dp0icons\backup.ico" "%~dp0_backup_to_GoogleDrive.py" --name="_backup_to_GoogleDrive"

pyinstaller -F --icon="%~dp0icons\restore.ico" "%~dp0_get_new_files_to_desktop.py" --name="_get_new_files_to_desktop"

robocopy .\dist .. *.exe
