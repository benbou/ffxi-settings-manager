import os
import subprocess
import shutil
from msvcrt import getch

import get_paths


def main():
    paths = get_paths.paths()

    # get Desktop path
    desktop_folder = paths["desktop_folder"]

    # get Google Drive sync path
    google_drive_folder = paths["google_drive_folder"]

    # get FFXI instalation path
    instalation_path = paths["ffxi_instalation_folder"]

    backup_FFXI_data_on_desktop(instalation_path, base_dst=desktop_folder)
    backup_FFXI_data_on_desktop(
        instalation_path, base_dst=desktop_folder, subfolder="ffxiuser"
    )
    # backup_FFXI_data_on_desktop(
    #     os.path.abspath(
    #         os.path.join(
    #             instalation_path, "..\\PlayOnlineViewer\\usr{}".format("\\")
    #         )
    #     ),
    #     base_dst=desktop_folder,
    #     subfolder="all",
    # )
    # www.reddit.com/r/ffxi/comments
    # /8u1uc6/more_than_4_pol_accounts_on_one_pol_boot_halp/

    counter = 0
    message = "{} hardlink created (Desktop folder -> Google Drive folder)"
    print("\r{}".format(message.format(counter)), end="")
    for root, dirs, files in os.walk(desktop_folder):
        relative_root = os.path.relpath(root, desktop_folder)
        # print("root: {}".format(root))
        # print(os.path.abspath(os.path.join(".", root)))
        # print("dirs: {}".format(len(dirs)))
        # print("files: {}".format(len(files)))
        # create folders
        if not os.path.exists(
            os.path.abspath(os.path.join(google_drive_folder, relative_root))
        ):
            os.mkdir(
                os.path.abspath(
                    os.path.join(google_drive_folder, relative_root)
                )
            )
        # create file hardlinks
        for file in [
            i
            for i in os.listdir(root)
            if os.path.isfile(os.path.abspath(os.path.join(root, i)))
        ]:
            if not os.path.exists(
                os.path.abspath(
                    os.path.join(google_drive_folder, relative_root, file)
                )
            ):
                result = subprocess.run(
                    'cmd.exe /c mklink /H "{}" "{}"'.format(
                        os.path.abspath(
                            os.path.join(
                                google_drive_folder, relative_root, file
                            )
                        ),
                        os.path.abspath(
                            os.path.join(desktop_folder, relative_root, file)
                        ),
                    ),
                    shell=True,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                )
                if result.returncode == 0:
                    counter += 1
                    print("\r{}".format(message.format(counter)), end="")
                    # out = result.stdout.decode("utf-8")
                    # print("stdout: {}".format(out))  # TODO log this
                else:
                    err = result.stderr.decode("utf-8")
                    print("stderr: {}".format(err))  # TODO log this
    print("")


def backup_FFXI_data_on_desktop(
        instalation_path, base_dst=".", subfolder="USER"):
    counter = 0
    message = "Backed up {} FFXI file(s) from '{}' folder"
    print("\r{}".format(message.format(counter, subfolder)), end="")
    for root, dirs, files in os.walk(
        os.path.join(instalation_path, subfolder)
    ):
        relative_root = root.replace(instalation_path, "")
        # print(relative_root)
        # os.path.getmtime(r"C:\Program Files (x86)\PlayOnline\SquareEnix"
        #                  r"\FINAL FANTASY XI\USER\101a568\ffxiusr.msg")

        # create folders
        if not os.path.exists(
            os.path.abspath(os.path.join(base_dst, relative_root))
        ):
            os.mkdir(os.path.abspath(os.path.join(base_dst, relative_root)))
        # copy new and updated files
        for file in files:
            source = os.path.abspath(os.path.join(root, file))
            destination = os.path.abspath(
                os.path.join(base_dst, relative_root, file)
            )

            if not os.path.exists(
                os.path.abspath(os.path.join(base_dst, relative_root, file))
            ):
                counter += 1
                print(
                    "\r{}".format(message.format(counter, subfolder)),
                    end="",
                )
                shutil.copy2(source, destination)
            else:
                ffxi_file_mod_time = os.path.getmtime(
                    source
                )  # return a unix timestamp   1572973467.4077163
                desktop_file_mod_time = os.path.getmtime(
                    destination
                )  # return a unix timestamp   1572973467.4077163
                if ffxi_file_mod_time > desktop_file_mod_time:
                    counter += 1
                    print(
                        "\r{}".format(message.format(counter, subfolder)),
                        end="",
                    )
                    shutil.copy2(source, destination)
    print("")


if __name__ == "__main__":
    main()
    print("Press any key to exit...")
    _ = getch()
