import os
import shutil
import subprocess
import sys
from msvcrt import getch

import admin
import get_paths


def main():
    paths = get_paths.paths()

    # get Desktop path
    desktop_folder = paths["desktop_folder"]

    # get Google Drive sync path
    google_drive_folder = paths["google_drive_folder"]

    # get FFXI instalation path
    instalation_path = paths["ffxi_instalation_folder"]

    counter = 0
    message = "{} hardlink created (Google Drive folder -> Desktop folder)"
    print("\r{}".format(message.format(counter)), end="")
    for root, dirs, files in os.walk(google_drive_folder):
        relative_root = os.path.relpath(root, google_drive_folder)
        # print("root: {}".format(root))
        # print(os.path.abspath(os.path.join(".", root)))
        # print("dirs: {}".format(len(dirs)))
        # print("files: {}".format(len(files)))
        # create folders, if needed
        if not os.path.exists(
            os.path.abspath(os.path.join(desktop_folder, relative_root))
        ):
            os.mkdir(
                os.path.abspath(os.path.join(desktop_folder, relative_root))
            )
        # create file hardlinks
        for file in os.listdir(root):
            current_file_path = os.path.abspath(
                os.path.join(google_drive_folder, relative_root, file)
            )
            current_dst_file_path = os.path.abspath(
                os.path.join(desktop_folder, relative_root, file)
            )
            if not os.path.exists(current_dst_file_path) and os.path.isfile(
                current_file_path
            ):
                result = subprocess.run(
                    'cmd.exe /c mklink /H "{}" "{}"'.format(
                        current_dst_file_path, current_file_path
                    ),
                    shell=True,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                )
                if result.returncode == 0:
                    counter += 1
                    print("\r{}".format(message.format(counter)), end="")
                    # out = result.stdout.decode("utf-8")
                    # print("stdout: {}".format(out))  # TODO log this
                else:
                    err = result.stderr.decode("utf-8")
                    print("stderr: {}".format(err))  # TODO log this
    print("")

    update_FFXI_data_in_installation_folder(
        instalation_path, base_src=google_drive_folder
    )
    update_FFXI_data_in_installation_folder(
        instalation_path, base_src=google_drive_folder, subfolder="ffxiuser"
    )
    # update_FFXI_data_in_installation_folder(
    #     os.path.abspath(
    #         os.path.join(
    #             instalation_path, "..\\PlayOnlineViewer\\usr{}".format("\\")
    #         )
    #     ),
    #     base_src=google_drive_folder,
    #     subfolder="all",
    # )


def update_FFXI_data_in_installation_folder(
        instalation_path, base_src=".", subfolder="USER"):
    # print("instalation path: {}".format(instalation_path))  # DEBUG
    # print("base src: {}".format(base_src))  # DEBUG
    # print("subfolder: {}".format(subfolder))  # DEBUG
    counter = 0
    message = "Updated {} FFXI file(s) in '{}' folder"
    print("\r{}".format(message.format(counter, subfolder)), end="")
    for root, dirs, files in os.walk(os.path.join(base_src, subfolder)):
        # print("root: {}".format(root))  # DEBUG
        relative_root = root.replace(base_src, "").strip("\\")
        # print("relative root: {}".format(relative_root))  # DEBUG
        # os.path.getmtime(r"C:\Program Files (x86)\PlayOnline\SquareEnix"
        #                  r"\FINAL FANTASY XI\USER\101a568\ffxiusr.msg")

        # create folders
        if not os.path.exists(
            os.path.abspath(os.path.join(instalation_path, relative_root))
        ):
            # print("about to create this folder: {}".format(
            #         os.path.join(instalation_path, relative_root)))  # DEBUG
            os.mkdir(
                os.path.abspath(os.path.join(instalation_path, relative_root))
            )
        # copy new and updated files
        for file in files:
            source = os.path.abspath(os.path.join(root, file))
            destination = os.path.abspath(
                os.path.join(instalation_path, relative_root, file)
            )

            if not os.path.exists(
                os.path.abspath(
                    os.path.join(instalation_path, relative_root, file)
                )
            ):
                counter += 1
                print(
                    "\r{}".format(message.format(counter, subfolder)),
                    end="",
                )
                shutil.copy2(source, destination)
            else:
                desktop_file_mod_time = os.path.getmtime(
                    source
                )  # return a unix timestamp   1572973467.4077163
                ffxi_file_mod_time = os.path.getmtime(
                    destination
                )  # return a unix timestamp   1572973467.4077163
                if desktop_file_mod_time > ffxi_file_mod_time:
                    counter += 1
                    print(
                        "\r{}".format(message.format(counter, subfolder)),
                        end="",
                    )
                    shutil.copy2(source, destination)
    print("")


# User must be admin to write in FFXI instalation folder
if not admin.isUserAdmin():
    """
    This if statement will produce the expected result ONLY if the
    program is frozen with PyInstaller. Otherwise the sys.executable will
    only return python.exe and will never actually run the code bellow.
        To workaround this:
            run the source as administrator, this will skip the if block.
    """
    if sys.executable.endswith("python.exe"):
        raise SystemError(
            "This application MUST be frozen "
            "with PyInstaller to work properly."
            "\nDo NOT run the source directly "
            "unless you have the Admin privileges."
        )
    admin.runAsAdmin(
        [sys.executable]
    )  # Launching the current executable whit Admin privilege.
    sys.exit()
else:
    main()
    print("Press any key to exit...")
    _ = getch()
